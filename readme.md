Erklärung zu den Folien

## 11-Fujitsu CE AI Portfolio (customer)
Dieser Foliensatz enthält alle Fujitsu CE Portfolio Element zu AI.

## 12-Vertriebsfoliensatz (intern)
Mit dieser Powerpoint Präsentation kann die Arbeitsweise des Digital Incubation Teams präsentiert werden. Ebenso sind aktuelle Usecases enthalten. Die Folien sind sowohl in Deutsch als auch in Englisch verfügbar.

## 13-AI intro and basics (intern)
Allgemeine Slides zu Thema AI

## 21-Vortraege (intern)
Hier liegen Folien zu bereits gehaltenen Vorträgen.

### 2020_08_FIS_Meeting
Meeting war am 28.08.2020.

### 2020_09_Vertical_Public
Wird im September 2020 stattfinden.
